## Laravel + Vue app

Simple quiz-like app I developed to learn Vue.

![app gif](autoportret.gif)

#### Installing
Clone the repository, run `composer install`, `npm install`, configure `.env` file, run `php artisan key:generate`, `php artisan migrate`, `php artisan db:seed`. Content in the app is only in Polish.
