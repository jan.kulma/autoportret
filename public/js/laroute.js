(function () {

    var laroute = (function () {

        var routes = {

            absolute: false,
            rootUrl: 'http://localhost',
            routes : [{"host":null,"methods":["GET","HEAD"],"uri":"api\/user","name":null,"action":"Closure"},{"host":null,"methods":["GET","HEAD"],"uri":"\/","name":null,"action":"Closure"},{"host":null,"methods":["GET","HEAD"],"uri":"portraits","name":"portraits.index","action":"App\Http\Controllers\PortraitController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"portraits\/create","name":"portraits.create","action":"App\Http\Controllers\PortraitController@create"},{"host":null,"methods":["POST"],"uri":"portraits","name":"portraits.store","action":"App\Http\Controllers\PortraitController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"portraits\/{portrait}","name":"portraits.show","action":"App\Http\Controllers\PortraitController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"portraits\/{portrait}\/edit","name":"portraits.edit","action":"App\Http\Controllers\PortraitController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"portraits\/{portrait}","name":"portraits.update","action":"App\Http\Controllers\PortraitController@update"},{"host":null,"methods":["DELETE"],"uri":"portraits\/{portrait}","name":"portraits.destroy","action":"App\Http\Controllers\PortraitController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"questions","name":"questions.index","action":"App\Http\Controllers\QuestionController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"questions\/create","name":"questions.create","action":"App\Http\Controllers\QuestionController@create"},{"host":null,"methods":["POST"],"uri":"questions","name":"questions.store","action":"App\Http\Controllers\QuestionController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"questions\/{question}","name":"questions.show","action":"App\Http\Controllers\QuestionController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"questions\/{question}\/edit","name":"questions.edit","action":"App\Http\Controllers\QuestionController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"questions\/{question}","name":"questions.update","action":"App\Http\Controllers\QuestionController@update"},{"host":null,"methods":["DELETE"],"uri":"questions\/{question}","name":"questions.destroy","action":"App\Http\Controllers\QuestionController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"answers","name":"answers.index","action":"App\Http\Controllers\AnswerController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"answers\/create","name":"answers.create","action":"App\Http\Controllers\AnswerController@create"},{"host":null,"methods":["POST"],"uri":"answers","name":"answers.store","action":"App\Http\Controllers\AnswerController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"answers\/{answer}","name":"answers.show","action":"App\Http\Controllers\AnswerController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"answers\/{answer}\/edit","name":"answers.edit","action":"App\Http\Controllers\AnswerController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"answers\/{answer}","name":"answers.update","action":"App\Http\Controllers\AnswerController@update"},{"host":null,"methods":["DELETE"],"uri":"answers\/{answer}","name":"answers.destroy","action":"App\Http\Controllers\AnswerController@destroy"}],
            prefix: '',

            route : function (name, parameters, route) {
                route = route || this.getByName(name);

                if ( ! route ) {
                    return undefined;
                }

                return this.toRoute(route, parameters);
            },

            url: function (url, parameters) {
                parameters = parameters || [];

                var uri = url + '/' + parameters.join('/');

                return this.getCorrectUrl(uri);
            },

            toRoute : function (route, parameters) {
                var uri = this.replaceNamedParameters(route.uri, parameters);
                var qs  = this.getRouteQueryString(parameters);

                if (this.absolute && this.isOtherHost(route)){
                    return "//" + route.host + "/" + uri + qs;
                }

                return this.getCorrectUrl(uri + qs);
            },

            isOtherHost: function (route){
                return route.host && route.host != window.location.hostname;
            },

            replaceNamedParameters : function (uri, parameters) {
                uri = uri.replace(/\{(.*?)\??\}/g, function(match, key) {
                    if (parameters.hasOwnProperty(key)) {
                        var value = parameters[key];
                        delete parameters[key];
                        return value;
                    } else {
                        return match;
                    }
                });

                // Strip out any optional parameters that were not given
                uri = uri.replace(/\/\{.*?\?\}/g, '');

                return uri;
            },

            getRouteQueryString : function (parameters) {
                var qs = [];
                for (var key in parameters) {
                    if (parameters.hasOwnProperty(key)) {
                        qs.push(key + '=' + parameters[key]);
                    }
                }

                if (qs.length < 1) {
                    return '';
                }

                return '?' + qs.join('&');
            },

            getByName : function (name) {
                for (var key in this.routes) {
                    if (this.routes.hasOwnProperty(key) && this.routes[key].name === name) {
                        return this.routes[key];
                    }
                }
            },

            getByAction : function(action) {
                for (var key in this.routes) {
                    if (this.routes.hasOwnProperty(key) && this.routes[key].action === action) {
                        return this.routes[key];
                    }
                }
            },

            getCorrectUrl: function (uri) {
                var url = this.prefix + '/' + uri.replace(/^\/?/, '');

                if ( ! this.absolute) {
                    return url;
                }

                return this.rootUrl.replace('/\/?$/', '') + url;
            }
        };

        var getLinkAttributes = function(attributes) {
            if ( ! attributes) {
                return '';
            }

            var attrs = [];
            for (var key in attributes) {
                if (attributes.hasOwnProperty(key)) {
                    attrs.push(key + '="' + attributes[key] + '"');
                }
            }

            return attrs.join(' ');
        };

        var getHtmlLink = function (url, title, attributes) {
            title      = title || url;
            attributes = getLinkAttributes(attributes);

            return '<a href="' + url + '" ' + attributes + '>' + title + '</a>';
        };

        return {
            // Generate a url for a given controller action.
            // laroute.action('HomeController@getIndex', [params = {}])
            action : function (name, parameters) {
                parameters = parameters || {};

                return routes.route(name, parameters, routes.getByAction(name));
            },

            // Generate a url for a given named route.
            // laroute.route('routeName', [params = {}])
            route : function (route, parameters) {
                parameters = parameters || {};

                return routes.route(route, parameters);
            },

            // Generate a fully qualified URL to the given path.
            // laroute.route('url', [params = {}])
            url : function (route, parameters) {
                parameters = parameters || {};

                return routes.url(route, parameters);
            },

            // Generate a html link to the given url.
            // laroute.link_to('foo/bar', [title = url], [attributes = {}])
            link_to : function (url, title, attributes) {
                url = this.url(url);

                return getHtmlLink(url, title, attributes);
            },

            // Generate a html link to the given route.
            // laroute.link_to_route('route.name', [title=url], [parameters = {}], [attributes = {}])
            link_to_route : function (route, title, parameters, attributes) {
                var url = this.route(route, parameters);

                return getHtmlLink(url, title, attributes);
            },

            // Generate a html link to the given controller action.
            // laroute.link_to_action('HomeController@getIndex', [title=url], [parameters = {}], [attributes = {}])
            link_to_action : function(action, title, parameters, attributes) {
                var url = this.action(action, parameters);

                return getHtmlLink(url, title, attributes);
            }

        };

    }).call(this);

    /**
     * Expose the class either via AMD, CommonJS or the global object
     */
    if (typeof define === 'function' && define.amd) {
        define(function () {
            return laroute;
        });
    }
    else if (typeof module === 'object' && module.exports){
        module.exports = laroute;
    }
    else {
        window.laroute = laroute;
    }

}).call(this);

