import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

import Index from 'root/components/index/Index';
import NewPortrait from 'root/components/portrait/create/Portrait';

const routes = [
	{ path: '/', name: 'index', component: Index },
	{ path: '/portrait/create', name: 'newPortrait', component: NewPortrait },
]

export default new VueRouter({routes});