import './bootstrap';
import router from './router/router.js';
import store from './store/store.js';
import App from './components/App';

const app = new Vue({
    el: '#app',
    router,
    store,
    components: { App }
});
