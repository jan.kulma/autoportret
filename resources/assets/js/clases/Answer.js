import store from 'root/store/store.js';
import personalityTypes from 'root/data/personalityTypes.js';

export default class Answer {

	static getAnswers() {
		if (false) {
			// get answers from vuex
		}

		return [];
	}

	static saveInVuex(answers) {
		store.commit('storeAnswers', answers);
	}

	static calcResultsForChart(answers) {
		return this.getArrayOfTypesScore(answers);
	}

	static calcResultsForSummary(answers) {
		let resultsWithPersonalityTypeAlphabeticKey = [];
		let results = this.getArrayOfTypesScore(answers);

		for (var i = 0; i < results.length; i++) {
			resultsWithPersonalityTypeAlphabeticKey.push({
				personalityType: personalityTypes[i].type,
				score: results[i]
			})
		}

		return _.orderBy(resultsWithPersonalityTypeAlphabeticKey, ['score'], ['desc']);

	}

	static getArrayOfTypesScore(answers) {
		let results = [];
		for (var i = 0; i < personalityTypes.length; i++) {
			let typePoints = 0;

			for (var j = 0; j < personalityTypes[i].questions.length; j++) {
				let questionId = personalityTypes[i].questions[j];
				if (answers[questionId]) {
					typePoints += parseInt(answers[questionId]);
				}
			}

			let typePercentage = (typePoints / personalityTypes[i].max) * 100;
			results.push(typePercentage);
		}

		return results;
	}
}

// let results = [

// 	{	
// 		type: 'A',
// 		questions: [1, 2],
// 		total: 0
// 	},

// 	{	
// 		type: 'B',
// 		questions: [2],
// 		total: 0
// 	},

// 	{	
// 		type: 'C',
// 		questions: [3],
// 		total: 0
// 	},

// ];













// constructor() {
// 	this.answer = null;
// 	this.portraitId = null;
// 	this.questionId = null;
// }

// setPortraitId(id) {
// 	this.portraitId = id;

// 	return this;
// }

// setQuestionId(id) {
// 	this.questionId = id;

// 	return this;
// }

// set answerValue(answer) {
// 	this.answer = answer;

// 	return this;
// }

// get() {
// 	if (! idsAreSet()) {
// 		return;
// 	}

// 	if (false) {
// 		// get from vuex
// 	}

// 	this.getFromDb();
// }

// getFromDb() {

// }

// save() {
// 	if (! idsAreSet()) {
// 		return;
// 	}

// 	axios.post(laroute.route('answers.store'), {
// 			portrait_id: this.portraitId,
// 			question_id: this.questionId,
// 			answer: this.answer
// 		})
// 		.then(response => {
// 			console.log(response)
// 		})
// 		.catch(error => {
// 			console.log(error)
// 		});
// }

// idsAreSet() {
// 	return this.portraitId && this.questionId;
// }