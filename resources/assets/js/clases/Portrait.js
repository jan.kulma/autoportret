import store from 'root/store/store.js';

export default class Portrait {

    constructor(id) {
        this.id = id;
    }

    static create() {
        return new Promise((resolve, reject) => this.createInDb(resolve, reject));
    }

    static createInDb(resolve, reject) {
        axios.post(laroute.route('portraits.store'))
            .then(response => {
                resolve(this.createNewInstance(response));
            })
            .catch(error => {
                reject(error);
            });
    }

    static createNewInstance(response) {
        let portrait = new Portrait(response.data.id);
        portrait.storeInVuex();

        return portrait;
    }

    storeInVuex() {
        store.commit('storePortrait', this);
    }

    //////////////////////////////////////

    isFinished() {
        return false;
    }

    static get(id) {
        return new Promise((resolve, reject) => {
            if (store.state.portrait) {
                return resolve(store.state.portrait);
            }

            return this.getFromDb(id, resolve, reject);
        });
    }

    static getFromDb(id, resolve, reject) {
        axios.get(laroute.route('portraits.show', {portrait: id}))
            .then(response => {
                resolve(this.createNewInstance(response));
            })
            .catch(error => {
                reject(error);
            });
    }
}