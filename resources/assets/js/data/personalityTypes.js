const personalityTypes = [

	{	
		type: 'A',
		questions: [53, 54, 56, 57, 79, 80, 84],
		max: 14
	},

	{	
		type: 'B',
		questions: [55, 58, 59, 60, 93, 94, 95],
		max: 14
	},

	{	
		type: 'C',
		questions: [12, 16, 53, 55, 57, 63, 88, 96, 97],
		max: 18
	},

	{
		type: 'D',
		questions: [19, 81, 83, 102, 103, 104, 105, 106],
		max: 16
	},

	{
		type: 'E',
		questions: [8, 9, 45, 51, 85, 89, 100, 101, 107],
		max: 18
	},

	{
		type: 'F',
		questions: [15, 46, 47, 48, 49, 50, 86, 87],
		max: 16
	},

	{
		type: 'G',
		questions: [10, 13, 14, 17, 18, 20, 24, 52, 78],
		max: 18
	},

	{
		type: 'H',
		questions: [61, 62, 64, 65, 66, 91, 92],
		max: 14
	},

	{
		type: 'I',
		questions: [22, 38, 39, 40, 41, 42, 43, 44],
		max: 16
	},

	{
		type: 'J',
		questions: [1, 2, 3, 4, 21, 23, 72, 99],
		max: 16
	},

	{
		type: 'K',
		questions: [5, 30, 32, 67, 68, 69, 70],
		max: 14
	},

	{
		type: 'L',
		questions: [11, 31, 73, 74, 75, 76, 77, 82],
		max: 16
	},

	{
		type: 'M',
		questions: [6, 7, 29, 33, 34, 35, 36, 37],
		max: 16
	},

	{
		type: 'N',
		questions: [25, 26, 27, 28, 71, 90, 98],
		max: 14
	}

];

export default personalityTypes;