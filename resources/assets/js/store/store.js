import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        questions: [],
        answers: []
    },
    mutations: {
        storeAnswers(state, answers) {
            state.answers = answers;
        }
    }
});

export default store;

