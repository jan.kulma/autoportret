<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
	const QUESTIONS = [
		1 => 'Mam zwyczaj spędzać w pracy więcej czasu niż koledzy i współpracownicy, ponieważ jestem perfekcjonistą i lubię, aby wszystko było wykonane bez zarzutu.',
		2 => 'Jestem osobą bardzo zorganizowaną. Lubię działać zgodnie z planem i przygotowuję sobie listy rzeczy do zrobienia. Zdarza mi się mieć ich tyle, że się w nich gubię!',
		3 => 'Czasem nazywają mnie "pracoholikiem". To prawda, że pracuję bardzo dużo, nawet wtedy, gdy mam dość pieniędzy i zapłacone wszystkie rachunki. Myślę, że gdybym naprawdę chciał, to mógłbym odłożyć choć na chwilę pracę i zrelaksować się.',
		4 => 'Lubie ustalony, rutynowy tryb zajęć i jeśli ktoś skłania mnie, bym go zmienił, potrafię być bardzo uparty.',
		5 => 'Nie znoszę rutyny i nie traktuję zbyt serio swoich zobowiązań, więc odkładam wiele spraw na tak długo, jak się da, lub w ogóle się do nich nie zabieram.',
		6 => 'Zauważyłem, że kiedy odnoszę sukces, to albo nie cieszy mnie to tak bardzo, jak powinno, albo też coś innego w moim życiu się nie wiedzie.',
		7 => 'Mam wiele talentów, z których nie umiem jednak czerpać korzyści. Mogę pomagać innym, ale nie potrafię wykorzystać moich umiejętności dla własnego pożytku.',
		8 => 'Mam dobrze rozwiniętą samoświadomość. Dokładnie wiem, co chcę robić, gdzie pracować, z jakimi ludźmi się przyjaźnić i jakie sprawy są dla mnie ważne.', 
		9 => 'Czasami czuję się tak, jakbym był pusty w środku, i nie potrafię dostrzec przed sobą żadnych celów.',
		10 => 'Lubię marzyć na jawie. Wyobrażam sobie, że jestem sławny, bogaty, potężny, uwielbiany przez tłumy za swoje zdolności czy urodę.',
		11 => 'Fascynuje mnie broń, przemoc, sztuki walki, nawet jeśli mam wątpliwości, czy to właściwe zainteresowania. Lubię filmy i programy telewizyjne z szybką akcją i ukazujące akty przemocy.',
		12 => 'Ludzie mówią, że wysławiam się w dziwny sposób - mówię rzeczy, które są dla nich niezrozumiałe, i nie wyjaśniam, o co mi chodzi.',
		13 => 'Niektórzy twierdza, że jestem arogancki, ale co mnie to obchodzi!',
		14 => 'Lubię być podziwiany, i potrafię nawet dopraszać się komplementów, kiedy mnie nie dostrzegają.',
		15 => 'Mój wygląd jest dla mnie bardzo ważny. Wiele czasu poświęcam, by wyglądać atrakcyjnie.',
		16 => 'Niektórzy uważają mnie za ekscentryka, bo ubieram się oryginalnie i zachowuję od nich dystans. To prawda, że żyję jakby w swoim własnym, małym świecie.',
		17 => 'Choć wydaje mi się, że rozumiem ludzi, zdarza mi się słyszeć od nich, że nie mam pojęcia, co naprawdę czują.',
		18 => 'Kiedy stoję w kolejce albo czekam na kelnera w zatłoczonej restauracji, zwykle próbuję się przepchnąć do przodu albo być obsłużonym natychmiast, - a w każdym razie mam poczucie, że mi się to należy.',
		19 => 'Nie miewam poczucie winy z powodu tego, co już zrobiłem.',
		20 => 'Mam poczucie wyższości nad otaczającymi mnie ludźmi. Spotykam się z wpływowymi osobami, należę do prestiżowych organizacji.',
		21 => 'Może ludzie uważają, że jestem sztywny i zbyt rygorystyczny. Ja jednak mocno wierzę, że nic nie usprawiedliwia zachowania niemoralnego lub nieetycznego.',
		22 => 'Przed podjęciem decyzji, nawet w powszednich sprawach, wolę zasięgnąć opinii innych.',
		23 => 'Nie potrafię niczego wyrzucić, nawet jeśli są to rzeczy niepotrzebne i nieważne',
		24 => 'Być może nieco przesadzam w ocenie ważności swojej osoby, ale uważam, że jestem naprawdę wiele wart.',
		25 => 'Jestem dla siebie bardzo surowym sędzią.',
		26 => 'Na pewno nie powiedziałbym o sobie, że jestem osobą lekkomyślną. Zazwyczaj wszystkim się przejmuję i wszystko traktuję poważnie.',
		27 => 'Czasami myślę, że poczucie winy leży w mojej naturze. Zdarza się, że sam nie wiem, dlaczego czuję się winny.',
		28 => 'Nie jestem zbyt pewny siebie i niekiedy mam poczucie, że jestem kompletnie bezwartościowy.',
		29 => 'Czuję się niezręcznie, pozwalając ludziom, aby mi pomagali, nawet jeśli nie waham się mówić im o swoich problemach.',
		30 => 'Ludzie uważają, że za bardzo się rozwodzę nad swoimi niepowodzeniami, ale mogliby zrozumieć, że naprawdę źle mi się wiedzie.',
		31 => 'Czasami śmieję się z cudzych niepowodzeń, choć trochę wstydzę się tej reakcji. Może to po prostu przekonanie, że "niezbadane są wyroki losu.',
		32 => 'Źle się czuję wśród ludzi, którzy nade mną górują. Często zazdroszczę im powodzenia.',
		33 => 'Czasem trudno jest mi rozluźnić się i dobrze się bawić. Kiedy pojawia się okazja rozrywki, coś mnie powstrzymuje przed jej wykorzystaniem.',
		34 => 'Czasem myślę, że jeśli chodzi o relacje z ludźmi, to sam jestem swoim najgorszym wrogiem. Raz po raz angażuję się w związek z partnerem, który traktuje mnie źle lub rozczarowuje. Trudno mi uwierzyć, że tak nietrafnie oceniam ludzi - chyba muszę być naiwny.',
		35 => 'Trudno ze mną wytrzymać przez dłuższy czas. Kiedy o tym myślę, wydaje mi się, że stawiam ludziom zbyt wygórowane wymagania. Mimo to martwi mnie, kiedy ludzie się na mnie złoszczą.',
		36 => 'Kiedy ktoś naprawdę się o mnie troszczy i okazuje mi wiele zrozumienia i czułości, zwykle przestaje mnie interesować. Chyba jest tak, że związek, w którym nie ma żadnego wyzwania, staje się dla mnie nudny.',
		37 => 'Czasem wydaje mi się, że zbyt wiele robię dla innych.',
		38 => 'Kluczowe decyzję pozostawiam zwykle innym, ważnym ludziom w moim życiu.',
		39 => 'Nie bywam motorem działania. Jestem chyba lepszym podwładnym niż liderem, ale za to jestem lojalnym członkiem zespołu.',
		40 => 'Lubię otwarcie przyznawać ludziom rację. Jeśli się z nimi nie zgadzam, to najczęściej zatrzymuję to dla siebie.',
		41 => 'Często rezygnuję z własnych planów, aby zrobić coś dla innych (nawet, jeśli to coś niezbyt przyjemnego). Wszystko po to, aby mnie lubili.',
		42 => 'Dobrze funkcjonuję, kiedy jestem z kimś związany. W samotności czuję się zupełnie bezradny.',
		43 => 'Kiedy rozstaję się z bliską osobą, wpadam w rodzaj paniki i gorączkowo rozglądam się za kimś innym.',
		44 => 'Być może za bardzo martwię się tym, co bym począł, gdybym utracił ważną dla mnie osobę.',
		45 => 'Czasem tak się boję, że ludzie mnie opuszczą, że aż dostaję bzika i domagam się, by mnie zapewniali o swojej wierności. To musi być dla nich mocno irytujące.',
		46 => 'Uwielbiam znajdować się w centrum uwagi - to bardzo podnosi mnie na duchu. Czuję się wtedy dużo lepiej, niż kiedy jestem na peryferia towarzystwa.',
		47 => 'Lubię flirtować i cieszy mnie, kiedy inni uważają mnie za osobę atrakcyjną seksualnie.',
		48 => 'Ludzie uważają mnie za osobę bardzo zajmującą. Umiem barwnie i zabawnie opowiadać o różnych wydarzeniach, choć nie zawsze ściśle trzymam się faktów.',
		49 => 'Jestem podatny na wpływy, Muszę mieć się na baczności, by nie pozwolić innym sobą kierować.',
		50 => 'O wiele zbyt często zakładam, że jestem z kimś związany bliżej, nie to ma miejsce w rzeczywistości. Bywa to dla mnie źródłem wielu cierpień.',
		51 => 'Moje związki z drugą osobą za zwykle bardzo intensywne, a uczucia wobec niej przechodzą z jednej skrajności w drugą. Czasem posuwam się niemal do bałwochwalstwa, kiedy indziej nie mogę jej znieść.',
		52 => 'Uczucie zazdrości jest dla mnie chlebem powszednim. Albo zazdroszczę komuś, albo podejrzewam, że ktoś zazdrości mnie.',
		53 => 'Choć chciałbym, nie potrafię ufać ludziom. Czuję, że jeśli nie będę ostrożny, inni mogą mnie wykorzystać.',
		54 => 'Czasem podejrzewam, że moi koledzy i przyjaciele nie są wobec mnie tak lojalni, jak bym oczekiwał.',
		55 => 'Właściwie to nie mam żadnych bliskich przyjaciół, z wyjątkiem może kilku osób z najbliższej rodziny.',
		56 => 'Jestem osobą zazdrosną. Wciąż się martwię, czy mój partner jest mi wierny.',
		57 => 'Jestem z natury osobą dość skrytą i większość spraw zatrzymuję dla siebie. Nigdy nie wiadomo, czy ktoś nie wykorzysta poufnych informacji dla własnych korzyści.',
		58 => 'Jestem samotnikiem i to mi nawet odpowiada. Niezbyt bawi mnie przebywanie w towarzystwie innych osób, nawet jeśli jest to ktoś z rodziny.',
		59 => 'Jeśli mam możliwość wyboru, to wolę wszystko robić sam.',
		60 => 'Nie odczuwam zbyt silnego popędu seksualnego.',
		61 => 'Trudno być mi sobą w bliskich związkach. Często wycofuję się z obawy, że mogę wydać się śmieszny.',
		62 => 'Jestem nieśmiały w kontaktach z nowo poznanymi ludźmi.',
		63 => 'Przebywanie w towarzystwie, nawet dobrze mi znanym, jest dla mnie często nie do zniesienia. Nie mogę wytrzymać uczucia, że inni patrzą na mnie i oceniają, nie zawsze w pochlebny sposób.',
		64 => 'Zwykle powstrzymuję się od bliższych związków z jakąś osoba, dopóki nie upewnię się o jej ciepłych uczuciach.',
		65 => 'Lepiej czuję się w pracy nie wymagającej zbyt wielu kontaktów z ludźmi. Boję się, że inni będą mnie krytykować.',
		66 => 'W sytuacjach towarzyskich czuję się pewnie. Nie mam zahamowań w rozmowie i nie boję się, że mógłbym powiedzieć coś głupiego lub okazać się ignorantem.',
		67 => 'Tak na prawdę to ludzie nie doceniają mnie i nie rozumieją.',
		68 => 'Bywam dość krytyczny wobec szefa czy innych ludzi mających władzę. Może nie zdaję sobie sprawy, co to znaczy być na ich miejscu, ale mam poczucie, że sam lepiej bym sobie dał radę.',
		69 => 'Kiedy ktoś prosi mnie, żebym zrobił coś, na co nie mam ochoty, potrafię być nieprzyjemny. Będę wykłócał się, zrzędził lub się obrażę.',
		70 => 'Jeśli ktoś mi się naprzykrza, mogę być nieznośny i uparty, ale później mam poczucie winy i próbuję jakoś temu zadośćuczynić.',
		71 => 'Chciałbym być mniej krytyczny wobec ludzi. Zwykle dostrzegam u wszystkich same wady.',
		72 => 'Niektórzy uważają, że za dużo chcę robić sam. Wolę jednak zrobić coś osobiście niż zlecić robotę komuś, kto ją wykona źle lub nie do końca. Mówią, że się rządzę, ale tak trzeba, żeby ludzie robili, co do nich należy, w sposób który uważam za właściwy.',
		73 => 'Jestem zdania, że silna dyscyplina jest czymś bardzo ważnym. Może nie jestem zdecydowanym zwolennikiem kar fizycznych, ale w zasadzie zgadzam się z powiedzeniem: "Jeden bity wart dwóch niebitych."',
		74 => 'Moi bliscy często narzekają, że ograniczam ich wolność i niezależność. Uważam jednak, że statek powinien mieć jednego kapitana.',
		75 => 'Ludzie skarżą się, że upokarzam ich w obecności innych. Nie powinni być tacy delikatni - od słów nikomu się krzywda nie dzieje, a w ogóle jeśli uważają, że jestem niesprawiedliwy, to powinni mi się przeciwstawić.',
		76 => 'Przypuszczam, że wzbudzam lęk w ludziach. Mówią mi czasem, że robią to, czego chcę, ponieważ mi się boją.',
		77 => 'W związkach z ludźmi lubię być osobą dominującą. Czasem bywam przez to niemiły lub nawet okrutny, nie zdając sobie z tego sprawy.',
		78 => 'Jestem zdania, że w pewnych sytuacjach trzeba nadepnąć komuś na odcisk, by osiągnąć swój cel.',
		79 => 'Moim zdaniem pewni ludzie popełniają drobne złośliwości, po to tylko, żeby mi dokuczyć, obrazić mnie lub zdenerwować.',
		80 => 'Jeśli ktoś potraktuje mnie źle, będę długo mu to pamiętał.',
		81 => 'Nie zawsze mówię prawdę.',
		82 => 'Od czasu do czasu wymyślam historyjki lub koloryzuję, by zobaczyć, jak inni to przyjmą. To tylko żarty, więc nikt nie powinien mieć mi tego za złe.',
		83 => 'Ludzie często mówią mi, że jestem zadziorny. To prawda, że jeśli ktoś nadepnie mi na odcisk, jestem gotów walczyć lub kłócić się do upadłego.',
		84 => 'Jestem wyczulony na krytykę, nawet zawoalowaną, i z pewnością nie pozostawię jej bez odpowiedzi.',
		85 => 'Mam okropny charakter, ale nic nie mogę na to poradzić.',
		86 => 'Czasem ludzie mówią mi, że nie wiedzą, kiedy traktować moje uczucia poważnie',
		87 => 'Wyrażam uczucia żywo i dramatycznie',
		88 => 'Mam własny sposób przeżywania uczuć. Na przykład potrafię dostrzec coś zabawnego w nieszczęściu i roześmiać się.',
		89 => 'Jestem bardzo podatny na zmiany nastroju. Nawet drobne rzeczy mogą wytrącić mnie z równowagi. Zdarza się, że w ciągu niewielu godzin doświadczam szerokiego wachlarza uczyć, od szczęśliwości do smutku, złości czy lęku. Ale zły nastrój nie trwa zbyt długo.',
		90 => 'Mam skłonność do zamartwiania się i smętnych rozmyślań.',
		91 => 'Będąc w towarzystwie, chciałbym mniej martwić się tym, co myślą o mnie inni ludzie.',
		92 => 'Nie cierpię próbować czegoś nowego i podejmować ryzyka, ponieważ boję się, że mógłbym zrobić z siebie durnia.',
		93 => 'Nie ma zbyt wielu rzeczy, które na prawdę lubię robić.',
		94 => 'Jestem typem "pokerzysty", w zasadzie nie okazuję emocji.',
		95 => 'Nie reaguję ani na krytykę, ani na komplementy ze strony otoczenia.',
		96 => 'Przypuszczam, że nadaje na innych falach niż większość ludzi. Pewne sprawy są dla mnie bardzo realne, choć nie można ich udowodnić. Na przykład wyczuwam obecność zmarłego członka rodziny lub mam poczucie przebywania poza własnym ciałem.',
		97 => 'Fascynują mnie takie sprawy, jak magia, postrzeganie pozazmysłowe oraz zjawiska nadprzyrodzone. Posiadam swego rodzaju "szósty zmysł" i zdarzało mi się mieć przeczucia, które później się sprawdzały.',
		98 => 'Jestem pesymistą. Szklanka jest dla mnie w połowie pusta, a nie w połowie pełna.',
		99 => 'Nie wydaję pieniędzy lekką ręką. Niektórzy mają mnie za skąpca, ale uważam, że zawsze trzeba mieć coś odłożone na czarną godzinę.',
		100 => 'Lubię działać pod wpływem impulsu, kiedy coś mnie porusza. Jestem w stanie upić się lub zaćpać, kiedy mam ochotę, folguję sobie przy stole, prowadzę samochód jak szalony albo robię zakupy bez opamiętania. Takie życie jest znacznie bardziej interesujące, choć oczywiście trzeba będzie kiedyś za to zapłacić.',
		101 => 'Kiedy jestem w złym humorze, zachowuję się teatralnie. Mogę nawet odgrażać się, że coś sobie zrobię, choć wcale nie mam takiego zamiaru.',
		102 => 'Fascynuje mnie życie poza prawem, gdzie można łamać wszelkie zasady i nie ponosić konsekwencji.',
		103 => 'Nie starcza mi cierpliwości na to, by długo pracować w jednym miejscu czy martwić się o pieniądze i rachunki, więc niektórzy uważają mnie za osobę nieodpowiedzialną.',
		104 => 'Nie jestem typem człowieka, który wybiera prostą i bezpieczną drogę. Potrafię ryzykować - przekraczać ograniczenia szybkości, prowadzić samochód po alkoholu - ale zawsze wiem, co robię, i osiągam swoje cele.',
		105 => 'Lubię działać spontanicznie, bez uprzedniego planowania.',
		106 => 'Jako dziecko byłem "z piekła rodem" i często wpadałem w tarapaty. Niektóre z wymienionych tu spraw odnoszą się do mnie: wagary, ucieczki z domu, bójki, przygody seksualne, kłamstwa, drobne kradzieże, znęcanie się nad kolegami, bezmyślne niszczenie cudzych rzeczy.',
		107 => 'Pod wpływem stresu mogę być nadmiernie podejrzliwy, albo zamykam oczy i uszy i udaję, że nic się nie dzieje.'


	];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$questions = [];
    	foreach (range(1, 107) as $questionId) {
    		$questions[] = [
    			'id' => $questionId,
    			'created_at' => Carbon::now(),
    			'updated_at' => Carbon::now()
    		];
    	}

        DB::table('questions')->insert($questions);

        $questionsTexts = [];
        foreach (range(1, 107) as $questionId) {
        	$questionsTexts[] = [
        		'id' => $questionId,
        		'question_id' => $questionId,
        		'language' => 'pl',
        		'text' => self::QUESTIONS[$questionId],
        		'created_at' => Carbon::now(),
        		'updated_at' => Carbon::now()
        	];
        }
        DB::table('question_texts')->insert($questionsTexts);
    }
}
