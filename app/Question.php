<?php

namespace App;

use App\Answer;
use App\QuestionText;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $with = ['texts'];

    public function texts()
    {
    	return $this->hasMany(QuestionText::class);
    }
}
