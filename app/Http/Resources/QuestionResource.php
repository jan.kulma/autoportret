<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class QuestionResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function($item, $key) {
            return [
                'id' => $item->id,
                'text' => $this->getLocalizedText($item)
            ];
        });

        return $this->collection->shuffle();
    }

    private function getLocalizedText($item)
    {
        $text = $item->texts->where('language', 'pl')->first();
        return $text ? $text->text : null;
    }
}
